extern crate proc_macro;
use proc_macro::TokenStream;
use quote::quote;
use std::{env::current_dir, path::PathBuf};
use syn::{
    parse::{Parse, ParseStream},
    parse_macro_input, LitStr,
};

struct Require {
    path: PathBuf,
}

impl Parse for Require {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        let path: LitStr = input.parse()?;
        Ok(Self {
            path: PathBuf::from(path.value()),
        })
    }
}

#[proc_macro]
pub fn require(input: TokenStream) -> TokenStream {
    let Require { path } = parse_macro_input!(input as Require);

    let mut current_level = current_dir().unwrap();
    while current_level.file_name().is_some() {
        let path = current_level.join("node_modules").join(&path);
        if path.exists() {
            let path = path.to_string_lossy();
            return quote! {
                include_bytes!(#path)
            }
            .into();
        }
        current_level.pop();
    }

    panic!("Could not find `{}`", path.display());
}
