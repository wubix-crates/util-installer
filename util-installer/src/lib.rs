use librust::{
    fs::{File, Permissions},
    io::Write,
    path::Path,
};
use require::require;
use wasm_bindgen::prelude::*;

const UTILITIES: [(&str, &[u8]); 10] = [
    ("/bin/cat", require!("coreutils/src/uu/cat/uu_cat.wef")),
    ("/bin/echo", require!("coreutils/src/uu/echo/uu_echo.wef")),
    (
        "/bin/chmod",
        require!("coreutils/src/uu/chmod/uu_chmod.wef"),
    ),
    (
        "/bin/false",
        require!("coreutils/src/uu/false/uu_false.wef"),
    ),
    (
        "/bin/mkdir",
        require!("coreutils/src/uu/mkdir/uu_mkdir.wef"),
    ),
    ("/bin/pwd", require!("coreutils/src/uu/pwd/uu_pwd.wef")),
    ("/bin/sh", require!("tinysh/tinysh.wef")),
    ("/bin/true", require!("coreutils/src/uu/true/uu_true.wef")),
    (
        "/usr/bin/upload-file",
        require!("upload-file/upload-file.wef"),
    ),
    ("/usr/bin/lolcat", require!("lolcat/lolcat.wef")),
];

const SET_USER_ID: [&str; 1] = ["upload-file"];

#[wasm_bindgen]
pub async fn start() {
    for (path, binary) in UTILITIES.iter() {
        let path = Path::new(path);
        let file_name = path.file_name().unwrap();

        let mut permissions: u32 = 0o755;
        if SET_USER_ID.iter().any(|&name| name == file_name) {
            permissions |= 0o4000;
        }

        librust::println!("Installing {}...", path.display()).await;
        let mut file = File::create(path).await.unwrap();
        file.set_permissions(Permissions::from_mode(permissions))
            .await
            .unwrap();
        file.write_all(binary).await.unwrap();
    }

    librust::println!("Installation completed!").await;
}
