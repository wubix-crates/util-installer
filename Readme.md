# `util-installer` is deprecated

The original purpose of `util-installer` was to unpack all core programs into
Wubix during the first boot. Since Wubix now has a package manager, [`wpm`],
`util-installer` is no longer needed.

[`wpm`]: https://gitlab.com/wubix-crates/wubix-package-manager
